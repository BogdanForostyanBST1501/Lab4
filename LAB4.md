Лабораторная работа № 4 

Одномерные клеточные автоматы 

Большинство программистов видели программы, которые имитируют "Жизнь".
Жизнь — это особый вид двухмерного клеточного автомата. В этом задании рассмотрим одномерный клеточный автомат (1dCA).
Вы увидите, что 1dCAs также может генерировать множество интересных моделей, в том числе и периодические и "хаотические" модели.
На самом деле, некоторые 1dCAs фактически являются универсальными моделями вычислений, но мы не будем вдаваться в это здесь. 

`1dCA` состоит из следующих элементов: 
    1. Массив ячеек, каждая из которых имеет состояние (представленное числом от 0 до максимального числа состояния минус 1). 
    2. Радиус окрестности, в котором говорится, сколько ячеек в любом направлении влияют на следующее состояние любой данной нам ячейки. Следующее состояние ячейки зависит не только от состояния этих ячеек, а также от её собственного состояния. 
    3. Таблица переходов состояний, определяющий состояние для установки данной ячейки, учитывая предыдущее её состояние и состояния ячеек в их окрестности.
    
=>1. Сначала напишу ` __init__. `в классе `1dCA`, чтобы указать значения (включая значения по умолчанию) двух чисел состояний, количества ближайших соседей и таблицы перехода состояния (которая по умолчанию должна быть None, что означает, что она будет генерироваться случайным образом). Также укажу размер массива ячеек   

    def __init__(self, cells_num=70, states_num=2, nearest_neighbors_num=1, transition_table=None, wraparound=True):
        if not isinstance(cells_num, (int, long)) or cells_num < 1:
            raise ValueError('Number of cells must be positive integer')
        if not isinstance(states_num, (int, long)) or cells_num < 2:
            raise ValueError('Number of states must be two or bigger')
        if not isinstance(nearest_neighbors_num, (int, long)) or nearest_neighbors_num < 0:
            raise ValueError('Number of nearest neighbors must be non negative integer')
        if not (transition_table is None):
            arr_err_msg = 'Transition table must be a list of integers'
        if not isinstance(transition_table, list):
            raise ValueError(arr_err_msg)
            else:
                for i in transition_table:
            if not isinstance(i, (int, long)):
                raise ValueError(arr_err_msg)
        if len(set(transition_table)) > states_num:
            raise ValueError('Passed number of states is bigger than number of ' + \'states in transition table')
    max_sum = (nearest_neighbors_num * 2 + 1) * (states_num - 1)
    if len(transition_table) - 1 < max_sum:
        raise ValueError('Length of transition table is lower than max sum')
        else:
            transition_table = self.generate_random_table(states_num, nearest_neighbors_num)

=>2. Затем напишу `generate_random_table`, который будет генерировать правильную таблицу переходов случайных состояний для данного числа соседей и состояний. 

    def generate_random_table(states_num=2, nearest_neighbors_num=1):
        max_sum = (nearest_neighbors_num * 2 + 1) * (states_num - 1)
        return [int(round(random() * (states_num - 1))) for _ in range(max_sum + 1)]

=>3. Потом `random_initialize`, которая будет случайным образом инициализировать каждую ячейку в массиве ячеек до допустимого состояния. 

    def random_initialize(self):
        self.cells = [int(round(random() * (self.states_num - 1))) for _ in range(len(self.cells))]
        
=>4. Далее  `update`, который будет использовать текущие значения массива ячеек для создания следующего набора значений. Для этого вам понадобится временный массив. Убедитесь, что вы не меняете исходный массив, пока новый массив не будет полностью вычислен. 

    def update(self):
        new_cells = []
        for i in range(len(self.cells)):
        if self.wraparound:
            start_i = i - self.nearest_neighbors_num
        if abs(start_i) > len(self.cells):
            start_i = start_i % len(self.cells)
        if i - self.nearest_neighbors_num < 0:
            start_i *= -1
            end_i = (i + self.nearest_neighbors_num) % len(self.cells)
            else:
                start_i = i - self.nearest_neighbors_num
                end_i = i + self.nearest_neighbors_num
                start_i = start_i if start_i >= 0 else 0
                end_i = end_i if end_i < len(self.cells) - 1 else len(self.cells) - 1
        if start_i < 0 or end_i < start_i:
            cells_sum = sum(self.cells[start_i + self.cells[:end_i+1])
            else:
                cells_sum = sum(self.cells[start_i:end_i+1])
                new_cells.append(self.transition_table[cells_sum])
                self.cells = new_cells
                
=>5.Затем функцию `get_states`, которая будет возвращать копию массива ячеек . 

    def get_states(self):
        return self.cells[]
        
=> 6. Функция `dump`, которая выведет массив ячеек на стандартный вывод. Если имеется не более 10 состояний, каждая ячейка  печатается как одна цифра в диапазоне [0-9] (без пробелов между цифрами).

    def dump(self):
        print ''.join(map(str, self.cells)) if self.states_num <= 10 else self.cells
        